﻿DROP DATABASE IF EXISTS b20190528b;
CREATE DATABASE b20190528b;
USE b20190528b;
/*hemos creado la base de datos y ahora vamos a crear las tablas*/
  CREATE TABLE profes (
    dni varchar (9),
    nombre varchar (100),
    telefono varchar (12),
    direccion varchar (100),
    PRIMARY KEY (dni)
    );

  CREATE TABLE modulos (
    codigo int AUTO_INCREMENT,
    nombre varchar (20),
    PRIMARY KEY (codigo)
    );
  CREATE TABLE alumnos (
    nºexp varchar (100),
    nombre varchar (20),
    apellidos varchar (40),
    fecnac varchar (10),
    delegado varchar (20),
    PRIMARY KEY (nºexp) );

  CREATE TABLE ser (
    delegado varchar(10),
    alumnos varchar (20),
    PRIMARY KEY (delegado,alumnos),
    UNIQUE KEY (alumnos),
    CONSTRAINT delegadoalumnos FOREIGN KEY (delegado) REFERENCES alumnos (nºexp),
    CONSTRAINT delegadoalumnos1 FOREIGN KEY (alumnos) REFERENCES alumnos (nºexp)
    );
  CREATE TABLE imparten (
    dniprofes varchar (100),
    codigomodulos int,
    PRIMARY KEY (dniprofes,codigomodulos),
    UNIQUE KEY (codigomodulos),
    CONSTRAINT fkimpartenprofes FOREIGN KEY (dniprofes) REFERENCES profes (dni),
    CONSTRAINT fkimpartenmodulos FOREIGN KEY (codigomodulos) REFERENCES modulos (codigo)
    );
  CREATE TABLE matriculado (
    codigomodulos int,
    nºexpedalum varchar (100),
    PRIMARY KEY (codigomodulos,nºexpedalum),
    CONSTRAINT fkmatriculadomodulos FOREIGN KEY (codigomodulos) REFERENCES modulos (codigo),
    CONSTRAINT fkmatriculadoalumnos FOREIGN KEY (nºexpedalum) REFERENCES alumnos (nºexp)
     );
  

